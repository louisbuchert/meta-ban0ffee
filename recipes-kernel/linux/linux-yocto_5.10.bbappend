FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
           file://bsp/ban0ffee_pi-standard.scc \
           file://patches/esp8089.scc \
           "

COMPATIBLE_MACHINE_ban0ffee-pi = "ban0ffee-pi"

KBRANCH_ban0ffee-pi  = "v5.10/standard/base"
SRCREV_machine_ban0ffee-pi ?= "8c516ced69f41563404ada0bea315a55bcf1df6f"
LINUX_VERSION_ban0ffee-pi = "5.10.21"

KERNEL_FEATURES += " \
                   esp8089.scc \
                   features/wifi/wifi-sdio.scc \
                   "
