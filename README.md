Ban0ffee-pi BSP, a virtual board using an Allwinner V3s.

It contains support for:
- ESP8089 wifi module
- OV2640 camera
- ILI9486 touchscreen

As the camera and the touchscreen share the same pins, they can't be used together.

local.conf:

MACHINE = "ban0ffee-pi"
DISTRO = "ban0ffee-pi-os"
WPA-SUPPLICANT_SSID = "your_ssid"
WPA-SUPPLICANT_PSK = "your_psk_generated_with_wpa_passphrase"

Dependancies : poky and meta-oe from meta-openembedded

Flash using bmaptool

Serial communication in 115200 8N1

